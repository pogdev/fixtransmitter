/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fixtransmitter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import quickfix.ConfigError;
import quickfix.DataDictionary;
import quickfix.FileLogFactory;
import quickfix.FileStoreFactory;
import quickfix.InvalidMessage;
import quickfix.LogFactory;
import quickfix.MessageStoreFactory;
import quickfix.RuntimeError;
import quickfix.Session;
import quickfix.SessionSettings;
import quickfix.ThreadedSocketInitiator;
import quickfix.field.*;
import quickfix.fix44.TradeCaptureReport;

/**
 *
 * @author henrikm
 */
public class FXMLDocumentController implements Initializable {

    private ThreadedSocketInitiator senderSession = null;
    private String m_senderCompID = "FIXUTIL_COMPID";
    private String m_targetCompID = "TRADESTORE_COMPID";

    private ObservableList<FIXTagValue> fixData = FXCollections.observableArrayList();

    @FXML
    private TextArea textTop;

    @FXML
    private TableView<FIXTagValue> tableFIXMsg;

    @FXML
    private TableColumn<FIXTagValue, String> colTag;

    @FXML
    private TableColumn<FIXTagValue, String> colValue;

    private quickfix.fix44.TradeCaptureReport buildMessage() {
        quickfix.fix44.TradeCaptureReport message =
                new quickfix.fix44.TradeCaptureReport();

        quickfix.fix44.TradeCaptureReport.NoSides sidesGroup =
                new quickfix.fix44.TradeCaptureReport.NoSides();
        sidesGroup.set(new Side(Side.BUY));
        sidesGroup.set(new Account("_000420_"));
        sidesGroup.set(new AcctIDSource(99));
        sidesGroup.set(new Currency("SEK"));
        message.addGroup(sidesGroup);

        sidesGroup.set(new Side(Side.SELL));
        sidesGroup.set(new Account("_000420_"));
        sidesGroup.set(new AcctIDSource(99));
        sidesGroup.set(new Currency("SEK"));
        message.addGroup(sidesGroup);


        quickfix.fix44.TradeCaptureReport.NoSecurityAltID instrGroup =
                new quickfix.fix44.TradeCaptureReport.NoSecurityAltID();
        instrGroup.set(new SecurityAltID("d79b579e-738d-11df-a86b-7147a59628fc"));
        instrGroup.set(new SecurityAltIDSource("999"));
        message.addGroup(instrGroup);
        instrGroup.set(new SecurityAltID("SE0000108656"));
        instrGroup.set(new SecurityAltIDSource("4"));
        message.addGroup(instrGroup);

        return message;
    }


    @FXML
    private void handleButtonSend(ActionEvent event) {
        String strData = textTop.getText();
        if (strData.length() == 0) {
            //strData = "8=FIX.4.49=35335=AE34=249=FIXUTIL_COMPID52=20120208-20:51:21.46656=TRADESTORE_COMPID487=0568=12570=Y454=2455=d79b579e-738d-11df-a86b-7147a59628fc456=999455=SE0000108656456=4552=254=21=_000420_660=9915=SEK381=1817313.751020=217705424=ee376564-3e75-4f91-afcc-12163da95c6454=11=_000420_660=9915=SEK381=1583195.91020=187495424=ee376564-3e75-4f91-afcc-12163da95c6410=232";
            strData = "8=FIX.4.49=57135=AE34=449=FIXUTIL_COMPID52=20120208-20:51:21.46656=TRADESTORE_COMPID15=SEK17=20121207-XSTO-B-B273731=79.2532=7048=SE000010381460=20121207-08:00:00.00064=2012120775=20121207207=XSTO487=0527=1a2c7d42-4044-11e2-b686-ef33fc71043e568=11570=Y571=1a2c7d42-4044-11e2-b686-ef33fc71043e856=45381=PACT9999=7165bd2e-224c-11e1-a808-04c21095de7d454=2455=1b89c7a2-2ddd-11df-8002-6f93c89d3e7e456=999455=SE0000103814456=4552=154=137=N/A1=ABC660=9915=SEK381=71405424=1ee61ff1-74b7-11e0-8d12-1c06b747deb3768=2769=20121207-08:00:00.000770=1769=20121207-08:00:00.000770=210=174";
        }
        if (senderSession.getSessions().size() == 1) {
            Session s = senderSession.getManagedSessions().get(0);
            DataDictionary dd = null;
            try {
                dd = new DataDictionary("dev_ai_ifix_dd.xml");
            } catch (ConfigError ex) {
                Logger.getLogger(FIXTransmitter.class.getName()).log(Level.SEVERE, null, ex);
            }
            quickfix.fix44.Message m = new quickfix.fix44.Message();
            //strData = "8=FIX.4.49=35335=AE34=249=FIXUTIL_COMPID52=20120208-20:51:21.46656=TRADESTORE_COMPID487=0568=12570=Y454=2455=d79b579e-738d-11df-a86b-7147a59628fc456=999455=SE0000108656456=4552=254=21=_000420_660=9915=SEK381=1817313.751020=217705424=ee376564-3e75-4f91-afcc-12163da95c6454=11=_000420_660=9915=SEK381=1583195.91020=187495424=ee376564-3e75-4f91-afcc-12163da95c6410=232";
            String tag = new String();
            String field = new String();
            //ObservableList<FIXTagValue> fixData = FXCollections.observableArrayList();
            for (String element : strData.split("\u0001")) {
                tag = element.split("=")[0];
                field = dd.getFieldName(Integer.parseInt(tag));

                //textBottom.appendText(field +"(" + tag  + ")=" + element.split("=")[1] + "\n");
                fixData.add(new FIXTagValue(field +"(" + tag  + ")", element.split("=")[1]));
            }
            tableFIXMsg.setItems(fixData);
            try {
                m.fromString(strData, dd, true);
            } catch (InvalidMessage ex) {
                Logger.getLogger(FIXTransmitter.class.getName()).log(Level.SEVERE, null, ex);
            }
            //m = buildMessage();
            s.send(m);
            System.out.println("sent message:" + m.toString());

        } else {
            System.out.println("Not just one session!");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Table view
        colTag.setCellValueFactory(new PropertyValueFactory<FIXTagValue, String>("tag"));
        colValue.setCellValueFactory(new PropertyValueFactory<FIXTagValue, String>("value"));
        tableFIXMsg.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        //FIX init
        //String fileName = "C:\\Users\\henrikm\\dev\\henrik\\FIXTransmitter\\FIXTransmittor_cf.txt";
        String fileName = "FIXTransmittor_cf.txt";
        // FooApplication is your class that implements the Application interface
        FIXSenderApp application = new FIXSenderApp();

        SessionSettings settings = null;
        try {
            settings = new SessionSettings(new FileInputStream(fileName));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ConfigError ex) {
            Logger.getLogger(FIXTransmitter.class.getName()).log(Level.SEVERE, null, ex);
        }
        MessageStoreFactory storeFactory = new FileStoreFactory(settings);
        LogFactory logFactory = new FileLogFactory(settings);

        quickfix.fix44.MessageFactory messageFactory = new quickfix.fix44.MessageFactory();
        //MessageFactory messageFactory = new DefaultMessageFactory();

        try {
            senderSession = new ThreadedSocketInitiator(application, storeFactory, settings, logFactory, messageFactory);
        } catch (ConfigError ex) {
            Logger.getLogger(FIXTransmitter.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            senderSession.start();
        } catch (ConfigError ex) {
            Logger.getLogger(FIXTransmitter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RuntimeError ex) {
            Logger.getLogger(FIXTransmitter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
