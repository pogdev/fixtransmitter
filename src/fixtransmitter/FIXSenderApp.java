/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fixtransmitter;

import quickfix.*;

/**
 *
 * @author henrikm
 */
public class FIXSenderApp implements quickfix.Application  {
  public void onCreate(SessionID sessionId){
      System.out.println("onCreate");
  }
  public void onLogon(SessionID sessionId){
      System.out.println("onLogon");
  }
  public void onLogout(SessionID sessionId){
      System.out.println("onLogout");
  }
  public void toAdmin(Message message, SessionID sessionId){
      System.out.println("toAdmin");
  }
  public void toApp(Message message, SessionID sessionId) throws DoNotSend {
      System.out.println("toApp");
  }
    
  public void fromAdmin(Message message, SessionID sessionId)
    throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon{
      System.out.println(FIXSenderApp.class.getName() + " fromAdmin");
      System.out.println(message.toString());
  }
  public void fromApp(Message message, SessionID sessionId)
    throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType{
      System.out.println(FIXSenderApp.class.getName() + " fromApp");
      System.out.println(message.toString());
  }
}
