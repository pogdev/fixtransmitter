 # default settings for sessions
[DEFAULT]
ConnectionType=initiator
ReconnectInterval=60
HeartBtInt=120
PersistMessages=N
UseDataDictionary=Y
FileLogPath=QFLog
#DataDictionary=dev_ai_ifix_dd.xml
FileStorePath=QFFileStore
ResetOnLogon=Y
StartTime=00:05:00
EndTime=23:55:00
SocketConnectHost=127.0.0.1

[SESSION]
BeginString=FIX.4.4
SenderCompID=FIXUTIL_COMPID
#SenderCompID=POSTEST
TargetCompID=TRADESTORE_COMPID
SocketConnectPort=9001
